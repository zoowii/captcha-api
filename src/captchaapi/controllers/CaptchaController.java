package captchaapi.controllers;

import captchaapi.models.CaptchaRequest;
import captchaapi.services.ICaptchaService;
import captchaapi.utils.StaticTextProducer;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zoowii.playmore.action.ActionResult;
import com.zoowii.playmore.action.BaseController;
import com.zoowii.playmore.annotation.Controller;
import com.zoowii.playmore.annotation.Route;
import com.zoowii.playmore.http.HttpServletRequestWrapper;
import com.zoowii.playmore.http.HttpServletResponseWrapper;
import nl.captcha.Captcha;
import nl.captcha.gimpy.BlockGimpyRenderer;
import nl.captcha.servlet.CaptchaServletUtil;
import org.apache.log4j.Logger;
import zuice.annotations.Autowired;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by zoowii on 15/2/12.
 */
@Controller
@Route("/captcha")
public class CaptchaController extends BaseController {
    private static final Logger LOG = Logger.getLogger(CaptchaController.class);

    @Autowired
    private ICaptchaService captchaService;

    @Route("")
    public ActionResult index() {
        LOG.info("visit dummy index");
        return ok("Hello, this is a very simple captcha service");
    }

    @Route("/requestCaptcha")
    public ActionResult requestCaptcha() {
        CaptchaRequest captchaRequest = captchaService.newCaptchaRequest();
        JSONObject captchaRequestJsonObject = new JSONObject();
        captchaRequestJsonObject.put("id", captchaRequest.getId());
        captchaRequestJsonObject.put("answer", captchaRequest.getAnswer());
        captchaRequestJsonObject.put("expireSeconds", captchaRequest.getExpireSeconds());
        String captchaRequestJson = JSON.toJSONString(captchaRequestJsonObject);
        LOG.info("requested a new captcha ${captchaRequestJson}");
        return ok(captchaRequestJson);
    }

    @Route("/view/:id")
    public ActionResult view(HttpServletResponseWrapper response, String id) {
        CaptchaRequest captchaRequest = CaptchaRequest.find.byId(id);
        if (captchaRequest == null) {
            return ok(String.format("Can't find captcha request #%s", id));
        }
        String text = captchaRequest.getAnswer();
        char[] chars = text.toCharArray();
        String finalText = "";
        for (char c : chars) {
            finalText += "" + c + "  ";
        }
        Captcha captcha = new Captcha.Builder(120, 48)
                .addText(new StaticTextProducer(finalText))
                .gimp(new BlockGimpyRenderer(3))
                .build();
        LOG.info("answer: " + captcha.getAnswer());
        CaptchaServletUtil.writeImage(response.getHttpServletResponse(), captcha.getImage());
        return null;
    }

    @Route(value = "/validate", methods = Route.ANY)
    public ActionResult validate(HttpServletRequestWrapper request) {
        String id = request.getParameter("id");
        String answer = request.getParameter("answer");
        CaptchaRequest captchaRequest = CaptchaRequest.find.byId(id);
        if (captchaRequest == null) {
            return ok(String.format("Can't find captcha request #%s", id));
        }
        Date now = new Date();
        Date expiresTime = captchaRequest.getCreatedTime();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(expiresTime);
        calendar.add(Calendar.SECOND, captchaRequest.getExpireSeconds());
        expiresTime = calendar.getTime();
        if (now.after(expiresTime)) {
            return ok("The captcha expired!");
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("success", captchaRequest.getAnswer().equalsIgnoreCase(answer));
        return ok(jsonObject);
    }
}
