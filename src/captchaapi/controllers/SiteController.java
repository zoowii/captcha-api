package captchaapi.controllers;

import com.zoowii.playmore.action.ActionResult;
import com.zoowii.playmore.action.BaseController;
import com.zoowii.playmore.annotation.Controller;
import com.zoowii.playmore.annotation.Route;

/**
 * Created by zoowii on 15/2/12.
 */
@Route("")
@Controller
public class SiteController extends BaseController {
    @Route({"", "/", "/home"})
    public ActionResult index() {
        return ok("This is home page os captchaapi service");
    }
}
