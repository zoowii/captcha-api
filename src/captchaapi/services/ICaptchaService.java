package captchaapi.services;

import captchaapi.models.CaptchaRequest;
import nl.captcha.text.producer.NumbersAnswerProducer;

/**
 * Created by zoowii on 15/2/12.
 */
public interface ICaptchaService {
    public CaptchaRequest newCaptchaRequest();
}
