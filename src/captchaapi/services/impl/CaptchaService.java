package captchaapi.services.impl;

import captchaapi.models.CaptchaRequest;
import captchaapi.services.ICaptchaService;
import com.zoowii.playmore.annotation.Service;
import nl.captcha.text.producer.NumbersAnswerProducer;
import nl.captcha.text.producer.TextProducer;

/**
 * Created by zoowii on 15/2/12.
 */
@Service
public class CaptchaService implements ICaptchaService {
    @Override
    public CaptchaRequest newCaptchaRequest() {
        TextProducer producer = new NumbersAnswerProducer(4);
        String text = producer.getText();
        CaptchaRequest captchaRequest = new CaptchaRequest();
        captchaRequest.setAnswer(text);
        captchaRequest.save();
        return captchaRequest;
    }
}
