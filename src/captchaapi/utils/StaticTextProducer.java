package captchaapi.utils;

import nl.captcha.text.producer.TextProducer;

/**
 * Created by zoowii on 15/2/12.
 */
public class StaticTextProducer implements TextProducer {
    private String text;

    public StaticTextProducer(String text) {
        this.text = text;
    }

    @Override
    public String getText() {
        return text;
    }
}
